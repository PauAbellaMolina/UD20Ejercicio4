package C2_UD20_T20_4.C2_UD20_T20_4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTextArea;

public class mainApp extends JFrame {

	private JPanel contentPane;
	private JTextArea mainTextArea;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					mainApp frame = new mainApp();
					frame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
				
			}
			
		});
		
	}

	public mainApp() {
		
		addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowActivated(WindowEvent e) {
				
				mainTextArea.setText(mainTextArea.getText() + "Ventana activada\n");
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				
				mainTextArea.setText(mainTextArea.getText() + "Ventana desactivada\n");
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				
				mainTextArea.setText(mainTextArea.getText() + "Ventana deiconificada\n");
				
			}
			
			@Override
			public void windowOpened(WindowEvent e) {
				
				mainTextArea.setText(mainTextArea.getText() + "Ventana abierta\n");
				
			}
			
		});
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel mainLabel = new JLabel("Eventos");
		mainLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		sl_contentPane.putConstraint(SpringLayout.NORTH, mainLabel, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, mainLabel, 10, SpringLayout.WEST, contentPane);
		contentPane.add(mainLabel);
		
		mainTextArea = new JTextArea();
		mainTextArea.setLineWrap(true);
		mainTextArea.setWrapStyleWord(true);
		mainTextArea.setEditable(false);
		sl_contentPane.putConstraint(SpringLayout.NORTH, mainTextArea, 6, SpringLayout.SOUTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, mainTextArea, 0, SpringLayout.WEST, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, mainTextArea, 212, SpringLayout.SOUTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.EAST, mainTextArea, 399, SpringLayout.WEST, mainLabel);
		contentPane.add(mainTextArea);
		
	}
}
